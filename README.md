# someone-api

This project also uses another repo for the swift app: someone: 29544286
Link: https://l.messenger.com/l.php?u=https%3A%2F%2Fgitlab.com%2Fedwardguil%2Fsomeone&h=AT3S63N-pzT5AHOmT-Yu4Wu_YZI-6CRIxoZ0-x_Z3sJtLZBfI9K_73OGuZOFuyAttlHjHZC3y93jtw8MXuS4t9EU0PHL7zfiMfC2u3XlfLSC9Fl78OB91gSxSJmU_YfqEPXMJA 

## Name
Someone-api
someone

## Description
This is the api for our swift app someone.

Unfortunaley, we didn't get to creating the the requirments.txt for the somone-api. However here is a list:

python=3.9.6
django=3.1.8=pypi_0
django-cors-headers=3.7.0
djangorestframework=3.12.0
pyjwt=2.1.0
openai


## Authors and acknowledgment
Thanks to the team, for creating this project, none of us had created a swift app before, krystyna and I (Blake) have never used the django framework or django rest api framework. 

Special thanks to Edward for building the swift app!
To Krystyna for helping with everything in between, querying someone, learning how to best to prompt someone for the best response, reading docs and implementing the content-filter to process the AI responses.

Blake Guilfoyle 
Edward Guilfoyle 
Krystyna Riabokon


##Run the api:
1. Clone rep
2. Install requirments
3. you will need to cd into the project directory: someoneApiParent and run: python manage.py runserver
4. Launch xcode and run the app
5. Have fun!

## License
For open source projects, say how it is licensed.

## Project status

